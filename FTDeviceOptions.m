//
//  DeviceiOSVersion.m
//  wallpapers
//
//  Created by Francesc Tovar on 24/09/12.
//  Copyright (c) 2012 Play Creatividad. All rights reserved.
//

#import "FTDeviceOptions.h"
#import "Social/Social.h"

@implementation FTDeviceOptions

+ (BOOL) iOSVersionIsBiggerThan:(int)version {
    // Se detecta el primer dígito de la versión del device (Ej: 5.1.1 solo cogerá 5)
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    currentVersion = [currentVersion substringWithRange:NSMakeRange(0, 1)];
    
    if ([currentVersion intValue] > version) {
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL) deviceIsIpad {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL)deviceIsPhone {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL)socialFacebookIsEnabled {
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
}

+ (BOOL)socialTwitterIsEnabled {
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
}

+ (BOOL)socialWeiboIsEnabled {
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeSinaWeibo];
}



@end
