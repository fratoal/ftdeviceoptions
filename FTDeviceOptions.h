//
//  FTDeviceOptions.h
//  wallpapers
//
//  Created by Francesc Tovar on 24/09/12.
//  Copyright (c) 2012 Play Creatividad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FTDeviceOptions : NSObject

// Devuelve YES si el valor actual es mayor al propuesto
// Solo mide versiones completas 4, 5, 6...
+ (BOOL)iOSVersionIsBiggerThan:(int)version;

// Devuelve YES si el dispositivo es un iPad
+ (BOOL)deviceIsIpad;

// Devuelve YES si el dispositivo es un iPhone o iPod Touch
+ (BOOL)deviceIsPhone;

+ (BOOL)socialFacebookIsEnabled;

+ (BOOL)socialTwitterIsEnabled;

@end